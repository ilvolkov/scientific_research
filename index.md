
# Работа

[Расписание](/diary/)

## Проекты

### Статьи

[h-BN fluorine modification](h-BN fluorine modification)
[CuFe,CuCo h-BN](CuFe,CuCo h-BN)

# [Workflow](Workflow)

Git
Hugo
[taskwarior](taskwarior)
taskwiki
vim

# База знаний

## Химия

### Неорганическая химия
### Органическая химия
### Коллоидная химия
### Катализ
#### Гомогенный катализ
#### Гетерогенный катализ

## Физика

## Математика

## Информатика
