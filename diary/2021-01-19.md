---

lang: ru
babel-lang: russian
output: pdf_document

---


# Обслуживание каталитической установки | project:equipment
* [X] Замена системы подачи газа (2021-01-18)  #7ddb5e6b
* [X] Обслуживание ChemBet (2021-01-18)  #bfaa1ecc
* [X] Обслуживание масспектрометра (2021-01-19)  #a878f39e
* [X] Установка программного обеспечения (2021-01-19)  #a7dc2abb
* [X] Проверочные испытания и наладка (2021-01-19)  #6f116388

# Аспирантура | project:phd
* [X] Аттестация (2021-01-19)  #d4c77109

# Статья NiCu/h-BN | project:paper-NiCu/h-BN
* [X] TEM исследования образцов (2021-01-20)  #279dde74

# Статья Fe(K,Na)/h-BN | project:paper-Fe(K,N)/h-BN
* [X] Синтез Fe2O3(K)/h-BN (2021-01-20)  #a560e0ff
* [X] Синтез Fe2O3(Na)/h-BN (2021-01-21)  #35641805
* [X] Структурные исследования полученных материалов (СЭМ, FTIR) (2021-01-22)  #eae7233a

# Статья FeZn/h-BN  | project:paper-FeZn/h-BN
* [X] Синтез FeZn/h-BN из растворов солей хлоридов железа и цинка (2021-01-25)  #6eed1c7a
* [X] Синтез FeZn/h-BN из растворов солей нитратов железа и цинка (2021-01-26)  #aecb4bf3
* [X] Структурные исследования FeZn/h-BN (2021-01-29)  #715c3319

# Статья Pt/h-BN single atom | project:paper-Pt/h-BN
* [X] Структурные исследования образов Pt/h-BN (2021-01-29)  #398dd81d
* [X] Синтез Pt/h-BN методом мокрой химии (слабым раствором хлороплатиновой кислоты)  #cb11a661
* [X] Нанесение платины методом осаждения из раствора с помощью инфузионного насоса (2021-01-27)  #c1cbfacb
* [X] Обработка полученных образцов в протоке водорода (2021-01-28)  #e5492af4
