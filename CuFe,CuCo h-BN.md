---
fontfamily: tnr
# pointsize: 10pt
# lineheight: 12pt
bibliography: /home/volk/Library/bibtex/lib.bib
title: Cu(Fe,Co)/h-BN nanohybrids for CO oxidation
author: Ilia Volkov
csl: /home/volk/Documents/Work/gost-csl/GOST-styles-for-Zotero/gost-r-7-0-5-2008-num-appear.csl
# papersize: a4
plot-configuration: /home/volk/.config/pandoc-plot/config.yaml
---

# Приготовление смеси для

Чистое железо на нитриде бора было приготовлено методом восстановления в растворе абсолютизированного этанола в ледяной бане. В качестве реагентов использовались хлорид железа 2 и тетраборат натрия.

$FeCl_2 * H_2O$

```{.run cmd="python" in="script" out="text"}
from chempy import balance_stoichiometry
reac, prod = balance_stoichiometry({'NaBH4', 'FeCl2:4H2O'}, {'Fe', 'NaCl', 'BH3', 'H2', H2O})
from pprint import pprint
pprint(dict(reac))
 ```
